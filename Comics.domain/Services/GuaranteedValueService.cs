﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IGuaranteedValueService
    {
        void CreateGuaranteedValue(Guid contractVersionId, string name, decimal unit, string targetValue, decimal lowerLimit, decimal upperLimit, string actualValue, decimal unitDeviation, decimal unitPrice, decimal CAP);
        void UpdateGuaranteedValue(Guid guaranteedValueId, string name, decimal unit, string targetValue, decimal lowerLimit, decimal upperLimit, string actualValue, decimal unitDeviation, decimal unitPrice, decimal CAP);
        void DeleteGuaranteedValue(Guid guaranteedValueId);
    }

    public class GuaranteedValueService : IGuaranteedValueService
    {
        IRepository repository;
        public GuaranteedValueService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateGuaranteedValue(Guid contractVersionId, string name, decimal unit, string targetValue, decimal lowerLimit, decimal upperLimit, string actualValue, decimal unitDeviation, decimal unitPrice, decimal CAP)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contractVersionId);
            GuaranteedValue GuaranteedValue = new GuaranteedValue(name, unit, targetValue, lowerLimit, upperLimit, actualValue, unitDeviation, unitPrice, CAP, currentContractVersion);
            repository.Add(GuaranteedValue);
        }

        public void UpdateGuaranteedValue(Guid guaranteedValueId, string name, decimal unit, string targetValue, decimal lowerLimit, decimal upperLimit, string actualValue, decimal unitDeviation, decimal unitPrice, decimal CAP)
        {
            var currentGuaranteedValue = repository.GetById<IGuaranteedValue>(guaranteedValueId);
            currentGuaranteedValue.Name = name;
            currentGuaranteedValue.Unit = unit;
            currentGuaranteedValue.TargetValue = targetValue;
            currentGuaranteedValue.LowerLimit = lowerLimit;
            currentGuaranteedValue.UpperLimit = upperLimit;
            currentGuaranteedValue.ActualValue = actualValue;
            currentGuaranteedValue.UnitDeviation = unitDeviation;
            currentGuaranteedValue.UnitPrice = unitPrice;
            currentGuaranteedValue.CAP = CAP;
            repository.Add(currentGuaranteedValue);
        }

        public void DeleteGuaranteedValue(Guid guaranteedValueId)
        {
            var currentGuaranteedValue = repository.GetById<IGuaranteedValue>(guaranteedValueId);
            repository.Remove(currentGuaranteedValue);
        }

    }
}
