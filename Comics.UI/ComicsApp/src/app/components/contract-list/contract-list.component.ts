import { Component, OnInit, ViewChild } from '@angular/core';
import { ContractService } from '../../services/contractService';
import { MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ContractDialogComponent } from '../contract/contract-dialog.component';
import { Router } from '@angular/router';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-contract',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.less']
})

export class ContractListComponent implements OnInit {

  public contractReference: string;
  public creationDate: string;
  public obj: Object;
  public contracts: any;
  public displayedColumns: string[] = ['reference', 'signatureDate', 'numberOfContractVersions'];
  public dataSource: any;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(public dialog: MatDialog, private contractService: ContractService) { }
  openDialog(): void {

    const dialogRef = this.dialog.open(ContractDialogComponent,
      {
        width: '250px',
        data: { contractReference: this.contractReference, creationDate: this.creationDate },

      });
    dialogRef.afterClosed().subscribe(
      result => {

        this.contractService.createContract(result);

        this.ngOnInit();
      },
      err => console.error(err),
      () => console.log('error saving contract')
    );
  }
  ngOnInit() {
    this.getContracts();
    
  }

  private getContracts() {
     
    this.contractService.getContracts().subscribe(
      data => {
        this.contracts = data.mappedContracts;
        console.log('done loading contracts')
        this.dataSource = new MatTableDataSource<any>(this.contracts);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      err => console.error(err),
      () => console.log('done loading contracts')
    );
  }

}
