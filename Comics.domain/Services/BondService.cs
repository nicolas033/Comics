﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IBondService
    {
        void CreateBond(Guid contractVersionId, string name, decimal value, DateTime startDate, DateTime endDate);
        void UpdateBond(Guid bondId, string name, decimal value, DateTime startDate, DateTime endDate);
        void DeleteBond(Guid bondId);
    }

    class BondService
    {
        IRepository repository;
        public BondService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateBond(Guid contractVersionId, string name, decimal value, DateTime startDate, DateTime endDate)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contractVersionId);
            Bond bond = new Bond(name, value, startDate, endDate, currentContractVersion);
            repository.Add(bond);
        }

        public void UpdateBond(Guid bondId, string name, decimal value, DateTime startDate, DateTime endDate)
        {
            var currentBond = repository.GetById<IBond>(bondId);
            currentBond.Name = name;
            currentBond.Value = value;
            currentBond.StartDate = startDate;
            currentBond.EndDate = endDate;
            repository.Add(currentBond);
        }

        public void DeleteBond(Guid bondId)
        {
            var currentBond = repository.GetById<IBond>(bondId);
            repository.Remove(currentBond);
        }
    }
}
