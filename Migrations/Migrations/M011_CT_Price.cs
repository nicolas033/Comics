﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(011)]
    public class M011_CT_Price : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("price")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_price")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("name").AsString()
                 .WithColumn("type").AsInt32()
                 .WithColumn("contractversionid").AsGuid().ForeignKey("fk_price_contractversion", "contractversion", "id");
        }
    }
}
