﻿using Comics.Domain.Entities.Common;
using System;

namespace Comics.Domain.Entities
{
    public interface IInvoice : IEntity
    {
        string Reference { get; set; }
        string Type { get; set; }
        decimal Value { get; set; } //ok
        DateTime Date { get; set; } //ok
        DateTime DueDate { get; set; } //ok
        string Status { get; set; } //ok
        IPaymentSchedule PaymentSchedule { get; set; }
        decimal PercentagePaid { get; set; } //ok
        decimal PercentageMSAchieved { get; set; } //ok
    }

    public class Invoice :Entity, IInvoice
    {
        protected Invoice()
        {

        }
        public Invoice(string reference, string type, decimal value, DateTime invoiceDate, DateTime dueDate, string status, IPaymentSchedule paymentSchedule, decimal percentagePaid, decimal percentageMSAchieved)
        {
            Reference = reference;
            Type = type;
            Value = value;
            Date = invoiceDate;
            DueDate = dueDate;
            Status = status;
            PaymentSchedule = paymentSchedule;
            PercentagePaid = percentagePaid;
            PercentageMSAchieved = percentageMSAchieved;
        }
        public virtual string Reference { get; set; }
        public virtual string Type { get; set; }
        public virtual decimal Value { get; set; } // [111-121-131]
        public virtual DateTime Date { get; set; } // [112-122-132]
        public virtual DateTime DueDate { get; set; } // [113-123-133]
        public virtual string Status { get; set; } // [114-124-134]
        public virtual IPaymentSchedule PaymentSchedule { get; set; } // Nullable
        public virtual decimal PercentagePaid { get; set; } // [115-125-135]
        public virtual decimal PercentageMSAchieved { get; set; } // [116-126-136]
    }
}
