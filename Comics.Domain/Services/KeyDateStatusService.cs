﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IKeyDateStatusService
    {
        void CreateKeyDateStatus(Guid keyDateId, DateTime estimationDate, DateTime bestEstimate);
        void UpdateKeyDateStatus(Guid keyDateStatusId, DateTime estimationDate, DateTime bestEstimate);
        void DeleteKeyDateStatus(Guid keyDateStatusId);
    }

    public class KeyDateStatusService : IKeyDateStatusService
    {
        IRepository repository;
        public KeyDateStatusService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateKeyDateStatus(Guid contractVersionId, DateTime estimationDate, DateTime bestEstimatee)
        {
            var currentKeyDate = repository.GetById<IKeyDate>(contractVersionId);
            KeyDateStatus KeyDateStatus = new KeyDateStatus(estimationDate, bestEstimatee, currentKeyDate);
            repository.Add(KeyDateStatus);
        }

        public void UpdateKeyDateStatus(Guid keyDateStatusId, DateTime estimationDate, DateTime bestEstimate)
        {
            var currentKeyDateStatus = repository.GetById<IKeyDateStatus>(keyDateStatusId);
            currentKeyDateStatus.EstimationDate = estimationDate;
            currentKeyDateStatus.BestEstimate = bestEstimate;
            repository.Add(currentKeyDateStatus);
        }

        public void DeleteKeyDateStatus(Guid keyDateStatusId)
        {
            var currentKeyDateStatus = repository.GetById<IKeyDateStatus>(keyDateStatusId);
            repository.Remove(currentKeyDateStatus);
        }

    }
}
