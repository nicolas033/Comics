﻿using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IClientKeyDateService
    {
        void CreateClientKeyDate(Guid contractVersionId);
        void UpdateClientKeyDate(Guid basePriceId);
        void DeleteClientKeyDate(Guid basePriceId);
    }

    class ClientKeyDateService
    {
        IRepository repository;
        public ClientKeyDateService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateClientKeyDate(Guid contractVersionId)
        {

        }

        public void UpdateClientKeyDate(Guid basePriceId)
        {

        }

        public void DeleteClientKeyDate(Guid basePriceId)
        {

        }
    }
}
