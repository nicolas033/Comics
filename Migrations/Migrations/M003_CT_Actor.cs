﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(003)]
    public class M003_CT_Actor : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("actor")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_actor")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("name").AsString()
                 .WithColumn("communicationcode").AsString()
                 .WithColumn("country").AsString()
                 .WithColumn("address").AsString()
                 .WithColumn("email").AsString()
                 .WithColumn("type").AsInt32()
                 .WithColumn("contractversionid").AsGuid().ForeignKey("fk_actor_contractversion", "contractversion", "id");
        }
    }
}
