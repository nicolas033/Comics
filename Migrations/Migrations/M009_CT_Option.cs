﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(009)]
    public class M009_CT_Option : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("option")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_option")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("name").AsString()
                 .WithColumn("date").AsDateTime()
                 .WithColumn("contractversionid").AsGuid().ForeignKey("fk_option_contractversion", "contractversion", "id");
        }
    }
}
