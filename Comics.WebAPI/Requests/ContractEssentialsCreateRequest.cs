﻿using Comics.Domain.Entities;
using Comics.Domain.Entities.Common;
using System;
using System.Collections.Generic;

namespace Comics.WebAPI.Requests
{
    public class ContractEssentialsCreateRequest
    {
        //Contract Definition
        public ContractType Type;
        public string Name;
        public DateTime SignatureDate;
        public string ClientName;
        public string ContractorName;
        //

        //Contract Key Dates
        public DateTime EffectiveDate;
        public List<KeyDate> ContractKeyDates;
        //

        //Client Key Dates
        public List<KeyDate> ClientKeyDates;
        //

        //Contractor Key Dates
        public List<KeyDate> ContractorKeyDates;
        //

        //Guaranteed Values
        public List<GuaranteedValue> GuaranteedValues;
        //

        //Bond
        public List<Bond> Bonds;
        //

        //Price
        public List<Price> BasePrices;
        public List<Price> OptionPrices;
        //
    }
}
