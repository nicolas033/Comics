﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(010)]
    public class M010_CT_PaymentSchedule : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("paymentschedule")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_paymentschedule")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("name").AsString()
                 .WithColumn("value").AsDecimal()
                 .WithColumn("currency").AsString()
                 .WithColumn("invoicingcondition").AsString()
                 .WithColumn("schedulecondition").AsString()
                 .WithColumn("paymentdate").AsDateTime()
                 .WithColumn("contractversionid").AsGuid().ForeignKey("fk_paymentschedule_contractversion", "contractversion", "id");
        }
    }
}
