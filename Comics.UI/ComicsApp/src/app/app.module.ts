import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppModuleRoutes } from './AppModuleRoutes';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuNavigationComponent } from './components/menu-navigation/menu-navigation.component';
import { SidebarModule } from 'ng-sidebar';
import { ContractVersionComponent } from './components/contract-version/contract-version.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ContractService } from './services/contractService';
import { HttpClientModule } from '@angular/common/http';
import { ContractDialogComponent } from './components/contract/contract-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatInputModule, MatButtonModule, MatTableModule, MatDatepickerModule, MatNativeDateModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { ContractComponent } from './components/contract/contract.component';
import { ContractListComponent } from './components/contract-list/contract-list.component';


@NgModule({

  declarations: [ 
    AppComponent,
    ContractListComponent,
    ContractVersionComponent,
    FooterComponent,
    MenuNavigationComponent,
    DashboardComponent,
    ContractDialogComponent,
    ContractComponent

  
  ],
  imports: [
    AppModuleRoutes,
    BrowserModule,
    SidebarModule.forRoot(),
    HttpClientModule,
    MatDialogModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatTableModule,
    FormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSortModule

  ],
  providers: [ContractService],
  bootstrap: [AppComponent],
  entryComponents: [ContractListComponent,ContractDialogComponent],

 
})
export class AppModule {

}
