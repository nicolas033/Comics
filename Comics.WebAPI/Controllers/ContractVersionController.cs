﻿using System;
using Comics.Domain.NHibernate;
using Comics.Domain.Services;
using Comics.WebAPI.Requests;
using Microsoft.AspNetCore.Mvc;

namespace Comics.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ContractVersionController : ControllerBase
    {
        private readonly IRepository repository;
        private readonly IContractVersionService contractVersionService;

        public ContractVersionController(IContractVersionService contractVersionService)
        {
            this.contractVersionService = contractVersionService;
        }

        [HttpPost]
        public ActionResult Create(Guid contractId,[FromBody] ContractVersionCreateRequest contractVersionData)
        {
            var type = contractVersionData.Type;
            var signatureDate = contractVersionData.SignatureDate;
            var name = contractVersionData.Name;
            contractVersionService.CreateContractVersion(contractId,type,name,signatureDate);

            return Ok("Contract Version successfully created");
        }


        [HttpPut]
        public ActionResult Update(Guid id, [FromBody] ContractVersionUpdateRequest contractVersionData)
        {
            var name = contractVersionData.Name;
            var type = contractVersionData.Type;
            var signatureDate = contractVersionData.SignatureDate;
            contractVersionService.UpdateContractVersion(id, type,name,signatureDate);

            return Ok("Contract Version successfully updated");
        }

        [HttpDelete]
        public ActionResult Delete(Guid id)
        {
            contractVersionService.DeleteContractVersion(id);
            return Ok("Contract version successfully deleted");
        }

       
    }
}