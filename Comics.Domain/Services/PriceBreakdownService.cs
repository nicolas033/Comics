﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IPriceBreakdownService
    {
        void CreatePriceBreakdown(Guid priceId, string name, decimal unitNumber, decimal unitPrice, string currencye);
        void UpdatePriceBreakdown(Guid priceBreakdownId, decimal unitNumber, decimal unitPrice, string currency);
        void DeletePriceBreakdown(Guid priceBreakdownId);
    }

    public class PriceBreakdownService : IPriceBreakdownService
    {
        IRepository repository;
        public PriceBreakdownService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreatePriceBreakdown(Guid priceId, string name, decimal unitNumber, decimal unitPrice, string currency)
        {
            var currentPrice = repository.GetById<IPrice>(priceId);
            PriceBreakdown PriceBreakdown = new PriceBreakdown(name,unitNumber, unitPrice, currency, currentPrice);
            repository.Add(PriceBreakdown);
        }

        public void UpdatePriceBreakdown(Guid priceBreakdownId, decimal unitNumber, decimal unitPrice, string currency)
        {
            var currentPriceBreakdown = repository.GetById<IPriceBreakdown>(priceBreakdownId);
            currentPriceBreakdown.UnitNumber = unitNumber;
            currentPriceBreakdown.UnitPrice = unitPrice;
            currentPriceBreakdown.Currency = currency;
            repository.Add(currentPriceBreakdown);
        }

        public void DeletePriceBreakdown(Guid priceBreakdownId)
        {
            var currentPriceBreakdown = repository.GetById<IPriceBreakdown>(priceBreakdownId);
            repository.Remove(currentPriceBreakdown);
        }

    }
}
