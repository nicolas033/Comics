﻿using System;
using System.Globalization;
using Comics.Domain.Entities.Common;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using Cfg = NHibernate.Cfg;

namespace Common.Utilities.NHibernate
{
    public class SessionFactoryConfiguration
    {
        public static ISessionFactory SessionFactory { get; set; }

        public static void InitializeSessionFactory(string connectionString)
        {

            var configuration = PostgreSQLConfiguration.PostgreSQL82.ConnectionString(connectionString);

            var sessionFactory = Fluently.Configure()
                .Database(configuration)
                .Mappings(m =>
                          m.HbmMappings
                              .AddFromAssemblyOf<IEntity>())
                .ExposeConfiguration(cfg =>
                {
                    cfg.SetProperty(Cfg.Environment.CommandTimeout, TimeSpan.FromMinutes(1).TotalSeconds.ToString(CultureInfo.InvariantCulture));
                })
                .BuildSessionFactory();            
            sessionFactory.OpenSession();          
            SessionFactory = sessionFactory;
        }
    }
}
