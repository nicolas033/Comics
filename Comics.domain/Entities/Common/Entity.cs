﻿using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Entities.Common
{
    public interface IEntity: IAggregateRoot
    {
        Guid Id { get; }
        DateTime CreationDateTime { get ;}
        string Hash { get; set; }
    }
    public abstract class Entity:IEntity
    {
        public virtual Guid Id { get; protected set; }
        public virtual DateTime CreationDateTime { get; protected set; }
        public virtual string Hash { get;  set; }
    }
}
