﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(004)]
    public class M004_CT_Bond : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("bond")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_bond")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("name").AsString()
                 .WithColumn("value").AsString()
                 .WithColumn("startdate").AsDateTime()
                 .WithColumn("enddate").AsDateTime()
                 .WithColumn("contractversionid").AsGuid().ForeignKey("fk_bond_contractversion","contractversion", "id");
        }
    }
}
