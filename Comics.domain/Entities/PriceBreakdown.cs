﻿using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IPriceBreakdown : IEntity
    {
        string Name { get; set; }
        decimal UnitNumber { get; set; } //can go to price
        decimal UnitPrice { get; set; } //can go to price
        //Todo: Currency if it's to represent dollar, euro then enumeration is better
        string Currency { get; set; } //can go to price
        IPrice Price { get; set; }
    }

   public class PriceBreakdown : Entity, IPriceBreakdown
    {
        private IPrice currentPrice;

        protected PriceBreakdown()
        {

        }
        public PriceBreakdown(string name, decimal unitNumber, decimal unitPrice, string currency, IPrice price)
        {
            Name = name;
            UnitNumber = unitNumber;
            UnitPrice = unitPrice;
            Currency = currency;
            Price = price;
        }

      
        public virtual string Name { get; set; }
        public virtual decimal UnitNumber { get; set; } // [8112-8122-8132]
        public virtual decimal UnitPrice { get; set; } // [8113-8123-8133]
        //Todo: Currency if it's to represent dollar, euro then enumeration is better
        public virtual string Currency { get; set; }
        public virtual IPrice Price { get; set; }
    }
}
