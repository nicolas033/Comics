﻿using Comics.Domain.Entities;
using System;
using Comics.Domain.NHibernate;
using Comics.Domain.Models;
using Comics.Domain.Common;
using System.Linq;

namespace Comics.Domain.Services
{
    public interface IContractService
    {
        void CreateContract(string reference, DateTime creationDate);
        void UpdateContract(Guid id);
        void DeleteContract(Guid ContractId);
        MappedContract GetContract(Guid ContractId);
        ContractResult GetAllContracts();
    }

    public class ContractService : IContractService
    {
        private readonly IRepository repository;
        private readonly IContractVersionService contractVersionService;
        public ContractService(IRepository repository,IContractVersionService contractVersionService)
        {
           this.repository = repository;
            this.contractVersionService = contractVersionService;
        }

        public void CreateContract(string reference, DateTime creationDate)
        {
            //var contratHash = Helper.GetHash(reference);
            var contratHash = "ContractHash";
            Contract contract = new Contract(reference, contratHash,creationDate);
            repository.Add<IContract>(contract);

            var contractVersionDate = creationDate;
            contractVersionService.CreateContractVersion(contract.Id, Entities.Common.ContractType.None, string.Empty, contractVersionDate);
        }
        public void DeleteContract(Guid id)
        {
            var currentContract = repository.GetById<IContract>(id);
            repository.Remove(currentContract);
        }
        public void UpdateContract(Guid id)
        {
            var currentContract = repository.GetById<IContract>(id);      
            var newContratHash = Helper.GetHash(string.Concat(currentContract.Reference));          
            currentContract.Hash = newContratHash;
            repository.Update(currentContract);
        }
        public MappedContract GetContract(Guid id)
        {
            var contract = repository.GetById<IContract>(id);
            var contractVersions = contract.Versions.ToList();
            var mappedContractVersions = contractVersions.Select(x => new MappedContractVersion
            {
                Name = x.Name,
                CreationDate = x.CreationDateTime.ToString("MM/dd/yyyy"),
                Version = x.Version,
                Type = x.Type
            });
            var mappedContract = new MappedContract
            {
                Reference = contract.Reference,
                SignatureDate = contract.CreationDateTime.ToString("MM/dd/yyyy"),
                ContractVersions = mappedContractVersions.ToList()
            };
            return mappedContract;
           
        }
        public ContractResult GetAllContracts()
        {
            var contracts = repository.GetAll<IContract>();
            var contractResult = new ContractResult();
            foreach(var contract in contracts)
            {
                var mappedContractVersions = contract.Versions.Select(x => new MappedContractVersion
                {
                    Name = x.Name,
                    CreationDate= x.CreationDateTime.ToLocalTime().ToString("MM/dd/yyyy"),
                    Version = x.Version,
                    Type = x.Type
                });
                var mappedContract = new MappedContract
                {
                    Reference = contract.Reference,
                    SignatureDate = contract.CreationDateTime.ToLocalTime().ToString("MM/dd/yyyy"),
                    ContractVersions = mappedContractVersions.ToList(),
                    NumberOfContractVersions = mappedContractVersions.ToList().Count()
                };
                contractResult.MappedContracts.Add(mappedContract);

           
        }
            return contractResult;
        }
    }
}
