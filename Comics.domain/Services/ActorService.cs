﻿using Comics.Domain.Entities;
using Comics.Domain.Entities.Common;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IActorService
    {
        void CreateActor(Guid contractVersionId, string name, string communicationCode, string country, string address, string email, ActorType type);
        void UpdateActor(Guid actorId, string name, string communicationCode, string country, string address, string email, ActorType type);
        void DeleteActor(Guid actorId);
    }

    public class ActorService : IActorService
    {
        IRepository repository;
        public ActorService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateActor(Guid contractVersionId, string name, string communicationCode, string country, string address, string email, ActorType type)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contractVersionId);
            Actor actor = new Actor(name, communicationCode, country, address, email, type, currentContractVersion);
            repository.Add(actor);
        }

        public void UpdateActor(Guid actorId, string name, string communicationCode, string country, string address, string email, ActorType type)
        {
            var currentActor = repository.GetById<IActor>(actorId);
            currentActor.Name = name;
            currentActor.CommunicationCode = communicationCode;
            currentActor.Country = country;
            currentActor.Address = address;
            currentActor.Email = email;
            currentActor.Type = type;
            repository.Add(currentActor);
        }

        public void DeleteActor(Guid actorId)
        {
            var currentActor = repository.GetById<IActor>(actorId);
            repository.Remove(currentActor);
        }

    }
}
