﻿using Comics.Domain.Entities.Common;
using System;

namespace Comics.WebAPI.Requests
{
    public class ContractVersionUpdateRequest
    {
        public string Name;
        public ContractType Type;
        public DateTime SignatureDate;
    }
}
