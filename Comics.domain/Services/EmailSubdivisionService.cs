﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IEmailSubdivisionService
    {
        void CreateEmailSubdivision(Guid actorId, Guid subdivisionId, string email, string toCc);
        void UpdateEmailSubdivision(Guid emailSubdivisionId, string email, string toCc);
        void DeleteEmailSubdivision(Guid emailSubdivisionId);
    }

    public class EmailSubdivisionService : IEmailSubdivisionService
    {
        IRepository repository;
        public EmailSubdivisionService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateEmailSubdivision(Guid actorId, Guid subdivisionId, string email, string toCc)
        {
            var currentActor = repository.GetById<IActor>(actorId);
            var currentSubdivision = repository.GetById<ISubdivision>(subdivisionId);
            EmailSubdivision EmailSubdivision = new EmailSubdivision(email, toCc, currentActor, currentSubdivision);
            repository.Add(EmailSubdivision);
        }

        public void UpdateEmailSubdivision(Guid emailSubdivisionId, string email, string toCc)
        {
            var currentEmailSubdivision = repository.GetById<IEmailSubdivision>(emailSubdivisionId);
            currentEmailSubdivision.Email = email;
            currentEmailSubdivision.ToCc = toCc;
            repository.Add(currentEmailSubdivision);
        }

        public void DeleteEmailSubdivision(Guid emailSubdivisionId)
        {
            var currentEmailSubdivision = repository.GetById<IEmailSubdivision>(emailSubdivisionId);
            repository.Remove(currentEmailSubdivision);
        }

    }
}
