﻿using Comics.Domain.Entities.Common;
using System;
using System.Collections.Generic;

namespace Comics.Domain.Entities
{
    public interface IContract : IEntity
    {
        string Reference { get;}
        IEnumerable<IContractVersion> Versions { get; }
        void AddContractVersion(IContractVersion version);
    }

    public class Contract : Entity,IContract
    {
        protected ISet<IContractVersion> versions;
        public virtual string Reference { get; protected set; } // [Protocol 131]
      protected Contract()
        { }
        public Contract(string reference, string contractHash, DateTime creationDate)
        {
            Reference = reference;
            CreationDateTime = creationDate.ToUniversalTime();
            Hash = contractHash;
            versions = new HashSet<IContractVersion>();
        }
        public virtual IEnumerable<IContractVersion> Versions => versions;

        public virtual void AddContractVersion(IContractVersion version)
        {
            versions.Add(version);
        }
    }  
}
