﻿using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IClientService
    {
        void CreateClient(Guid contractVersionId);
        void UpdateClient(Guid clientId);
        void DeleteClient(Guid clientId);
    }

    class ClientService
    {
        IRepository repository;
        public ClientService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateClient(Guid contractVersionId)
        {

        }

        public void UpdateClient(Guid clientId)
        {

        }

        public void DeleteClient(Guid clientId)
        {

        }
    }
}
