﻿namespace Comics.Domain.NHibernate
{
    public interface ISessionContext
    {
        object this[string key]
        {
            get;
            set;
        }
    }
}
