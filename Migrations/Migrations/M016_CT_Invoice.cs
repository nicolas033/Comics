﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(016)]
    public class M016_CT_Invoice : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("invoice")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_invoice")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("reference").AsString()
                 .WithColumn("type").AsString()
                 .WithColumn("invoicedate").AsDateTime()
                 .WithColumn("duedate").AsDateTime()
                 .WithColumn("status").AsString()
                 .WithColumn("percentagepaid").AsDecimal()
                 .WithColumn("percentagemsachieved").AsDecimal()
                 .WithColumn("paymentscheduleid").AsGuid().ForeignKey("fk_invoice_paymentschedule","paymentschedule", "id");
        }

    }
}
