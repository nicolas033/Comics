﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IOptionService
    {
        void CreateOption(Guid contractVersionId, string name, DateTime date);
        void UpdateOption(Guid optionId, string name, DateTime date);
        void DeleteOption(Guid optionId);
    }

    public class OptionService : IOptionService
    {
        IRepository repository;
        public OptionService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateOption(Guid contractVersionId, string name, DateTime date)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contractVersionId);
            Option Option = new Option(name, date, currentContractVersion);
            repository.Add(Option);
        }

        public void UpdateOption(Guid optionId, string name, DateTime date)
        {
            var currentOption = repository.GetById<IOption>(optionId);
            currentOption.Name = name;
            currentOption.Date = date;
            repository.Add(currentOption);
        }

        public void DeleteOption(Guid optionId)
        {
            var currentOption = repository.GetById<IOption>(optionId);
            repository.Remove(currentOption);
        }

    }
}
