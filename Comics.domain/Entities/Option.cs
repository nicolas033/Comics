﻿using System;
using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IOption : IEntity
    {   
        string Name { get; set; }
        DateTime Date { get; set; }
        IContractVersion ContractVersion { get; set; }
    }
    public class Option:Entity,IOption
    {
        protected Option()
        {

        }
        public Option(string name, DateTime date, IContractVersion contractVersion)
        {
            Name = name;
            Date = date;
            ContractVersion = contractVersion;
        }
        public virtual string Name { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual IContractVersion ContractVersion { get; set; }
    }
}
