﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(008)]
    public class M008_CT_KeyDateStatus : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("keydatestatus")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_keydatestatus")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("estimationdate").AsDateTime()
                 .WithColumn("bestestimate").AsDateTime()
                 .WithColumn("keydateid").AsGuid().ForeignKey("fk_keydatestatus_keydate","keydate", "id");       
        }
    }
}
