﻿using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IBasePriceService
    {
        void CreateBasePrice(Guid contractVersionId);
        void UpdateBasePrice(Guid basePriceId);
        void DeleteBasePrice(Guid basePriceId);
    }

    class BasePriceService
    {
        IRepository repository;
        public BasePriceService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateBasePrice(Guid contractVersionId)
        {
            
        }

        public void UpdateBasePrice(Guid basePriceId)
        {
            
        }

        public void DeleteBasePrice(Guid basePriceId)
        {
            
        }
    }
}
