using System;
using System.Collections.Generic;
using NHibernate;

namespace Common.Utilities.NHibernate
{
    public interface IRepository
    {
        void Add<T>( T t ) where T : IAggregateRoot;
        void AddNonRoot<T>( T t );
        void Remove<T>( T t ) where T : IAggregateRoot;
        void RemoveNonRoot<T>( T t );
        IQueryResult<T> Query<T>( IQuery<T> query );
    }

    public interface ICacheRepository : IRepository
    {
        void ClearFromCache( object objectToClear );
    }

    public interface IQuery<out T>
    {
        IQueryResult<T> Execute( ISession session );
    }

    public interface IQueryResult<out T>
    {
        IUniqueQueryResult<T> UniqueResult();
        IEnumerable<T> List();
    }

    public interface IUniqueQueryResult<out T>
    {
        T IfNotFound( Action action );
        T IfNotFoundDoNothing();
        T IfNotFoundThrow<TE>() where TE: Exception, new();
        T IfFound( Action action );
        T IfFoundThrow<TE>( ) where TE : Exception, new();
    }

    public interface IAggregateRoot
    {
    }
}