﻿using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IGuaranteedValue : IEntity
    {
        string Name { get; set; } //ok
        decimal Unit { get; set; } //ok 
        string TargetValue { get; set; } //ok
        decimal LowerLimit { get; set; } //ok
        decimal UpperLimit { get; set; } //ok
        string ActualValue { get; set; } //ok
        decimal UnitDeviation { get; set; } //ok
        decimal UnitPrice { get; set; } //ok
        decimal CAP { get; set; } //ok
        IContractVersion ContractVersion { get; set; }
    }

    public class GuaranteedValue: Entity,IGuaranteedValue
    {
        protected GuaranteedValue()
        {
           
        }
        public GuaranteedValue(string name, decimal unit, string targetValue, decimal lowerLimit, decimal upperLimit, string actualValue, decimal unitDeviation, decimal unitPrice, decimal cap, IContractVersion contractVersion)
        {
            Name = name;
            Unit = unit;
            TargetValue = targetValue;
            LowerLimit = lowerLimit;
            UpperLimit = upperLimit;
            ActualValue = actualValue;
            UnitDeviation = unitDeviation;
            UnitPrice = unitPrice;
            CAP = cap;
            ContractVersion = contractVersion;
        }
        public virtual string Name { get; set; }
        public virtual decimal Unit { get; set; }
        public virtual string TargetValue { get; set; }
        public virtual decimal LowerLimit { get; set; }
        public virtual decimal UpperLimit { get; set; }
        public virtual string ActualValue { get; set; }
        public virtual decimal UnitDeviation { get; set; } // [6'11-6'21-6'31]
        public virtual decimal UnitPrice { get; set; } // [6'12-6'22-6'32]
        public virtual decimal CAP { get; set; } // [6'13-6'23-6'33]
        public virtual IContractVersion ContractVersion { get; set; }
    }
}
