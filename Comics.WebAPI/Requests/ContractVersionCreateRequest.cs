﻿using Comics.Domain.Entities.Common;
using System;
using System.Collections.Generic;

namespace Comics.WebAPI.Requests
{
    public class ContractVersionCreateRequest
    {
        public ContractType Type;
        public DateTime SignatureDate;
        public string Name;
        public List<ActorCreateRequest> ActorCreateRequest;
        public List<SubdivisionCreateRequest> SubdivisionCreateRequest;
        public List<OptionCreateRequest> OptionCreateRequest;
        public List<ContractCodeCategoryCreateRequest> ContractCodeCategoryCreateRequest;
        public List<BondCreateRequest> BondCreateRequest;
        public List<GuaranteedValueCreateRequest> GuaranteedValueCreateRequest;
        public List<KeyDateCreateRequest> KeyDateCreateRequest;
        public List<PaymentScheduleCreateRequest> PaymentScheduleCreateRequest;
    }
    public class KeyDateCreateRequest
    {
        string Name { get; set; } 
        KeyDateType Type { get; set; } 
        DateTime DueDate { get; set; } 
        DateTime LimitDate { get; set; } 
        DateTime UnitDelay { get; set; } 
        decimal UnitPrice { get; set; } 
        decimal CAP { get; set; } 
        DateTime ActualDate { get; set; }
        List<KeyDateStatusCreateRequest> KeyDateStatusCreateRequest { get; }
    }
    public class BondCreateRequest
    {
        string Name { get; set; } 
        decimal Value { get; set; } 
        DateTime StartDate { get; set; } 
        DateTime EndDate { get; set; }
    }
    public class ActorCreateRequest
    {
        string Name { get; set; }
        string CommunicationCode { get; set; }
        string Country { get; set; }
        string Address { get; set; }
        string Email { get; set; }
        ActorType Type { get; set; }
        List<RepresentationCreateRequest> RepresentationCreateRequest { get; }
    }
    public class SubdivisionCreateRequest
    {
        string Title { get; set; }
        List<EmailSubdivisionCreateRequest> EmailSubdivisions { get; }
    }
    public class OptionCreateRequest
    {
        string Name { get; set; }
        DateTime Date { get; set; }
    }
    public class ContractCodeCategoryCreateRequest
    {
        string Name { get; set; }
        string Code { get; set; }
    }
    public class GuaranteedValueCreateRequest
    {
        string Name { get; set; }
        decimal Unit { get; set; }
        string TargetValue { get; set; }
        decimal LowerLimit { get; set; }
        decimal UpperLimit { get; set; }
        string ActualValue { get; set; }
        decimal UnitDeviation { get; set; }
        decimal UnitPrice { get; set; }
        decimal CAP { get; set; }
    }
    public class PaymentScheduleCreateRequest
    {
        string Name { get; set; }
        decimal Value { get; set; }
        string Currency { get; set; }
        string InvoicingCondition { get; set; }
        string ScheduleCondition { get; set; }
        DateTime PaymentDate { get; set; }
    }
    public class KeyDateStatusCreateRequest
    {
        DateTime EstimationDate { get; set; }
        DateTime BestEstimate { get; set; }
    }
    public class RepresentationCreateRequest
    {
        string Title { get; set; }
        string Email { get; set; }
    }
    public class EmailSubdivisionCreateRequest
    {
        string Email { get; set; }
        string ToCc { get; set; }
    }

}
