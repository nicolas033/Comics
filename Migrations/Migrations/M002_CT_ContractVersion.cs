﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(002)]
    public class M002_CT_ContractVersion : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("contractversion")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_contractversion")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("version").AsInt32()
                 .WithColumn("type").AsString()
                 .WithColumn("name").AsString()               
                 .WithColumn("contractid").AsGuid().ForeignKey("fk_contract_contractversion", "contract", "id");
        }
    }
}
