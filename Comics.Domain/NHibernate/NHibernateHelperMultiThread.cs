using System;
using System.Diagnostics;
using NHibernate;

namespace Comics.Domain.NHibernate
{
    public static class NHibernateHelperMultiThread
    {
        private static ISessionFactory sessionFactory;
        private static ISessionContext sessionContext;


        public static void Initialize( ISessionContext context, ISessionFactory factory )
        {
            sessionContext = context;
            sessionFactory = factory;
        }

        public static ISession GetCurrentSession(string sessionKey)
        {
            var session = ( ISession )sessionContext[sessionKey];
            if ( session == null )
            {
                session = sessionFactory.OpenSession();
                try
                {
                    session.EnableFilter("noAdministratorFilter");
                }
                catch ( Exception )
                { }

                sessionContext[sessionKey] = session;
            }
            return session;
        }

        public static ISession DisableFilters( this ISession sessionWithFilters )
        {
            sessionWithFilters.DisableFilter( "noAdministratorFilter" );
            return sessionWithFilters;
        }

        public static void CloseCurrentSession(string sessionKey)
        {
            GetCurrentSession(sessionKey).Dispose();
            sessionContext[sessionKey] = null;
        }

        public static void BeginTransaction(string sessionKey)
        {
            GetCurrentSession(sessionKey).BeginTransaction();
        }

        public static void CommitTransaction(string sessionKey)
        {
            var transaction = GetCurrentSession(sessionKey).Transaction;
            if ( transaction != null )
            {
                transaction.Commit();
                transaction.Dispose();
            }
        }

        public static void RollbackTransaction(string sessionKey)
        {
            var transaction = GetCurrentSession(sessionKey).Transaction;
            if ( transaction != null )
            {
                try
                {
                    transaction.Rollback();
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e);
                }

                transaction.Dispose();
            }
        }

        public static void EndSession(string sessionKey)
        {
            CloseCurrentSession(sessionKey);
        }

        public static void Evict( object entity, string sessionKey )
        {
            if ( sessionContext != null )
            {
                var session = ( ISession ) sessionContext[ sessionKey ];
                if ( session != null)
                {
                    session.Evict( entity );
                }
            }
        }
    }

    public static class NHibernateLoggerMultiThread
    {
        public enum IgnoreCommit
        {
            True, False
        }

        private static ISessionFactory sessionFactory;
        private static ISessionContext sessionContext;
        private static IgnoreCommit ignoringCommit;

        public static void Initialize(ISessionContext context, ISessionFactory factory)
        {
            ignoringCommit = IgnoreCommit.False;
            sessionContext = context;
            sessionFactory = factory;
        }

        public static void Initialize(ISessionContext context, ISessionFactory factory, IgnoreCommit ignoreCommit)
        {
            ignoringCommit = ignoreCommit;
            sessionContext = context;
            sessionFactory = factory;
        }

        public static ISession GetCurrentSession(string sessionKey)
        {
            if (sessionContext[sessionKey] == null)
            {
                var session = sessionFactory.OpenSession();
                sessionContext[sessionKey] = session;
            }
            return (ISession)sessionContext[sessionKey];
        }

        private static void CloseCurrentSession(string sessionKey)
        {
            GetCurrentSession(sessionKey).Dispose();
            sessionContext[sessionKey] = null;
        }

        public static void BeginTransaction(string sessionKey)
        {
            GetCurrentSession(sessionKey).BeginTransaction();
        }

        public static void CommitTransaction(string sessionKey)
        {
            if (ignoringCommit == IgnoreCommit.True)
            {
                return;
            }
            var transaction = GetCurrentSession(sessionKey).Transaction;
            if (transaction != null)
            {
                transaction.Commit();
                transaction.Dispose();
            }
            CloseCurrentSession(sessionKey);
        }

        public static void RollbackTransaction(string sessionKey)
        {
            var transaction = GetCurrentSession(sessionKey).Transaction;
            if (transaction != null && transaction.IsActive)
            {
                transaction.Rollback();
                transaction.Dispose();
            }

            CloseCurrentSession(sessionKey);
        }
    }
}