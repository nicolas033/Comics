﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(012)]
    public class M012_CT_PriceBreakdown : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("pricebreakdown")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_pricebreakdown")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("name").AsString()
                 .WithColumn("unitnumber").AsDecimal()
                 .WithColumn("unitprice").AsDecimal()
                 .WithColumn("currency").AsString()
                 .WithColumn("priceid").AsGuid().ForeignKey("fk_pricebreakdown_price", "price", "id");
        }
    }
}
