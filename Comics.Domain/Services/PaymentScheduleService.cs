﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IPaymentScheduleService
    {
        void CreatePaymentSchedule(Guid contractVersionId, string name, decimal value, string currency, string paymentCondition, string scheduleCondition, DateTime paymentDate);
        void UpdatePaymentSchedule(Guid paymentScheduleId, string name, decimal value, string currency, string paymentCondition, string scheduleCondition, DateTime paymentDate);
        void DeletePaymentSchedule(Guid paymentScheduleId);
    }

    public class PaymentScheduleService : IPaymentScheduleService
    {
        IRepository repository;
        public PaymentScheduleService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreatePaymentSchedule(Guid contractVersionId, string name, decimal value, string currency, string paymentCondition, string scheduleCondition, DateTime paymentDate)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contractVersionId);
            PaymentSchedule PaymentSchedule = new PaymentSchedule(name, value, currency, paymentCondition, scheduleCondition, paymentDate, currentContractVersion);
            repository.Add(PaymentSchedule);
        }

        public void UpdatePaymentSchedule(Guid paymentScheduleId, string name, decimal value, string currency, string paymentCondition, string scheduleCondition, DateTime paymentDate)
        {
            var currentPaymentSchedule = repository.GetById<IPaymentSchedule>(paymentScheduleId);
            currentPaymentSchedule.Name = name;
            currentPaymentSchedule.Value = value;
            currentPaymentSchedule.Currency = currency;
          //  currentPaymentSchedule.PaymentCondition = paymentCondition;
            currentPaymentSchedule.ScheduleCondition = scheduleCondition;
            currentPaymentSchedule.PaymentDate = paymentDate;
            repository.Add(currentPaymentSchedule);
        }

        public void DeletePaymentSchedule(Guid paymentScheduleId)
        {
            var currentPaymentSchedule = repository.GetById<IPaymentSchedule>(paymentScheduleId);
            repository.Remove(currentPaymentSchedule);
        }

    }
}
