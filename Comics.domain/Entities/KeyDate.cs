﻿using Comics.Domain.Entities.Common;
using System;
using System.Collections.Generic;

namespace Comics.Domain.Entities
{
    public interface IKeyDate : IEntity
    {
        string Name { get; set; } //ok
        KeyDateType Type { get; set; } //ok
        DateTime DueDate { get; set; } //ok Target Date
        DateTime LimitDate { get; set; } //ok
        DateTime UnitDelay { get; set; } //ok
        decimal UnitPrice { get; set; } //ok
        decimal CAP { get; set; } //ok
        DateTime ActualDate { get; set; }
        IEnumerable<IKeyDateStatus> KeyDatesStatus { get;  }
        IContractVersion ContractVersion { get; set; }

    }

    public class KeyDate: Entity, IKeyDate
    {
        protected ISet<IKeyDateStatus> keyDatesStatus;
        protected KeyDate()
        {
            keyDatesStatus = new HashSet<IKeyDateStatus>();
        }
        public KeyDate(string name, KeyDateType type, DateTime dueDate, DateTime limitDate, DateTime unitDelay,
            decimal unitPrice, decimal cap, DateTime actualDate, IContractVersion contractVersion)
        {
            Name = name;
            Type = type;
            LimitDate = limitDate;
            DueDate = dueDate;
            UnitDelay = unitDelay;
            UnitPrice = unitPrice;
            CAP = cap;
            ActualDate = actualDate;
            ContractVersion = contractVersion;
        }
        public virtual string Name { get; set; } // [2211-2221-2231-311-321-331-411-421-431]
        public virtual DateTime LimitDate { get; set; } // [313-323-333]
        public virtual DateTime UnitDelay { get; set; } // [611-621-631-6'11-6'21-6'31]
        public virtual decimal UnitPrice { get; set; } // [612-622-632-6'12-6'22-6'32]
        public virtual decimal CAP { get; set; } // [613-623-633-6'13-6'23-6'33]
        public virtual KeyDateType Type { get; set; }
        public virtual DateTime ActualDate { get; set; }
        public virtual IEnumerable<IKeyDateStatus> KeyDatesStatus => keyDatesStatus;
        public virtual IContractVersion ContractVersion { get; set; }
        public virtual DateTime DueDate { get; set; }
    }
}
