﻿using System.Runtime.Serialization;

namespace Comics.Domain.Entities.Common
{
   public enum PriceType
    {
        [EnumMember]
        Base = 0,
        [EnumMember]
        Option = 1
    }

    public enum ActorType
    {
        [EnumMember]
        Client = 0,
        Contractor = 1
    }
    public enum KeyDateType
    {
        [EnumMember]
        Contract = 0,
        Client = 1,
        Contractor = 2
    }
    public enum ContractType
    {
        [EnumMember]
        None = 0,
        EPC = 1,
        EP = 2,
        E = 3
    }
}
