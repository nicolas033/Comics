﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Comics.Domain.NHibernate
{
    public class NHibernateRepositoryFilter : ActionFilterAttribute
    {
        public static string[] TransactionMethods = { "POST", "PUT", "DELETE" };
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.TraceIdentifier = Guid.NewGuid().ToString();

            if (TransactionMethods.Contains(context.HttpContext.Request.Method))
            {
                NHibernateHelperMultiThread.BeginTransaction(context.HttpContext.TraceIdentifier);
            }
        }

        public override void OnResultExecuted(ResultExecutedContext context)
        {
            if (TransactionMethods.Contains(context.HttpContext.Request.Method))
            {
                if (context.HttpContext.Response.StatusCode >= 200 && context.HttpContext.Response.StatusCode <= 299)
                {
                    try
                    {
                        NHibernateHelperMultiThread.CommitTransaction(context.HttpContext.TraceIdentifier);
                    }
                    catch (Exception)
                    {
                        NHibernateHelperMultiThread.RollbackTransaction(context.HttpContext.TraceIdentifier);
                        NHibernateHelperMultiThread.CloseCurrentSession(context.HttpContext.TraceIdentifier);
                        throw;
                    }

                }
                else
                {
                    NHibernateHelperMultiThread.RollbackTransaction(context.HttpContext.TraceIdentifier);
                }
            }
            NHibernateHelperMultiThread.CloseCurrentSession(context.HttpContext.TraceIdentifier);
        }
    }
}