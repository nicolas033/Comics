﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(013)]
    public class M013_CT_Representation : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("representation")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_representation")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("title").AsString()
                 .WithColumn("email").AsString()
                 .WithColumn("actorid").AsGuid().ForeignKey("fk_representation_actor", "actor", "id");
        }
    }
}
