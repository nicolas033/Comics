﻿using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IPrice : IEntity
    {
        string Name { get; set; } //ok
        PriceType Type { get; set; } //ok
        IContractVersion ContractVersion { get; set; }
    }

    public class Price : Entity, IPrice
    {
        protected Price()
        {

        }
        public Price(string name, PriceType type, IContractVersion contractVersion)
        {
            Name = name;
            Type = type;
            ContractVersion = contractVersion;
        }
        public virtual string Name { get; set; } // [8111-8121-8131]
        public virtual PriceType Type { get; set; }
        public virtual IContractVersion ContractVersion { get; set; }
    }
}
