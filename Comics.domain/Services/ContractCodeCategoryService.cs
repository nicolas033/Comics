﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IContractCodeCategoryService
    {
        void CreateContractCodeCategory(Guid contractVersionId, string name, string code);
        void UpdateContractCodeCategory(Guid ContractCodeCategoryId, string name, string code);
        void DeleteContractCodeCategory(Guid ContractCodeCategoryId);
    }

    public class ContractCodeCategoryService : IContractCodeCategoryService
    {
        IRepository repository;
        public ContractCodeCategoryService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateContractCodeCategory(Guid contractVersionId, string name, string code)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contractVersionId);
            ContractCodeCategory ContractCodeCategory = new ContractCodeCategory(name, code, currentContractVersion);
            repository.Add(ContractCodeCategory);
        }

        public void UpdateContractCodeCategory(Guid ContractCodeCategoryId, string name, string code)
        {
            var currentContractCodeCategory = repository.GetById<IContractCodeCategory>(ContractCodeCategoryId);
            currentContractCodeCategory.Name = name;
            currentContractCodeCategory.Code = code;
            repository.Add(currentContractCodeCategory);
        }

        public void DeleteContractCodeCategory(Guid ContractCodeCategoryId)
        {
            var currentContractCodeCategory = repository.GetById<IContractCodeCategory>(ContractCodeCategoryId);
            repository.Remove(currentContractCodeCategory);
        }

    }
}
