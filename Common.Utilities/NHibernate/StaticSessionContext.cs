using System.Collections.Generic;

namespace Common.Utilities.NHibernate
{
    public class StaticSessionContext : ISessionContext
    {
        private readonly Dictionary<string, object> dictionary = new Dictionary<string, object>();

        public object this[string key]
        {
            get
            {
                return dictionary.ContainsKey( key ) ? dictionary[key] : null;
            }
            set
            {
                if ( dictionary.ContainsKey( key ) )
                {
                    if (value == null)
                    {
                        dictionary.Remove(key);
                    }
                    else
                    {
                        dictionary[key] = value;
                    }
                }
                else if (value != null)
                {
                    dictionary.Add( key, value );
                }
            }
        }
    }
}