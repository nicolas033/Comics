﻿using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IContractKeyDateService
    {
        void CreateContractKeyDate(Guid contractVersionId);
        void UpdateContractKeyDate(Guid contractKeyDateId);
        void DeleteContractKeyDate(Guid contractKeyDateId);
    }

    class ContractKeyDateService
    {
        IRepository repository;
        public ContractKeyDateService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateContractKeyDate(Guid contractVersionId)
        {

        }

        public void UpdateContractKeyDate(Guid contractKeyDateId)
        {

        }

        public void DeleteContractKeyDate(Guid contractKeyDateId)
        {

        }
    }
}
