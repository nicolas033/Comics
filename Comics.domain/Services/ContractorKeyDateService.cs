﻿using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IContractorKeyDateService
    {
        void CreateContractorKeyDate(Guid contractVersionId);
        void UpdateContractorKeyDate(Guid contractorKeyDateId);
        void DeleteContractorKeyDate(Guid contractorKeyDateId);
    }

    class ContractorKeyDateService
    {
        IRepository repository;
        public ContractorKeyDateService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateContractorKeyDate(Guid contractVersionId)
        {

        }

        public void UpdateContractorKeyDate(Guid contractorKeyDateId)
        {

        }

        public void DeleteContractorKeyDate(Guid contractorKeyDateId)
        {

        }
    }
}
