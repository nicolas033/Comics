﻿using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IOptionPriceService
    {
        void CreateOptionPrice(Guid contractVersionId);
        void UpdateOptionPrice(Guid optionPriceId);
        void DeleteOptionPrice(Guid optionPriceId);
    }

    class OptionPriceService
    {
        IRepository repository;
        public OptionPriceService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateOptionPrice(Guid contractVersionId)
        {

        }

        public void UpdateOptionPrice(Guid optionPriceId)
        {

        }

        public void DeleteOptionPrice(Guid optionPriceId)
        {

        }
    }
}
