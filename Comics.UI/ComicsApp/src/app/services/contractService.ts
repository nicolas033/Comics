
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ContractService {

  constructor(private http: HttpClient) { }

  public getContracts() {
    return this.http.get('http://localhost:5000/api/Contract/GetAll');
  }
  public createContract(data) {
    var body = {
      Reference: data.contractReference,
      SignatureDate: data.creationDate
    };

    this.http.post("http://localhost:5000/api/Contract/Create", JSON.stringify(body), httpOptions).subscribe();
  }
}

