﻿using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IContractCodeCategory : IEntity
    {
        string Name { get; set; }
        string Code { get; set; }
        IContractVersion ContractVersion { get; set; }
    }

    public class ContractCodeCategory : Entity,IContractCodeCategory
    {
        protected ContractCodeCategory()
        {
        }
        public ContractCodeCategory(string name, string code, IContractVersion contractVersion)
        {
            Name = name;
            Code = code;
            ContractVersion = contractVersion;
        }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual IContractVersion ContractVersion { get; set; }

    }
}
