﻿using System;
using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IBond : IEntity
    {
        string Name { get; set; } //ok
        decimal Value { get; set; } //ok
        DateTime StartDate { get; set; } //ok
        DateTime EndDate { get; set; } //ok
        IContractVersion ContractVersion { get; set; }
    }

    public class Bond : Entity,IBond
    {
        #region Contructor
        protected Bond()
        {
        }
        public Bond(string name, decimal value, DateTime startDate, DateTime endDate, IContractVersion contractVersion)
        {
            Name = name;
            Value = value;
            StartDate = startDate;
            EndDate = endDate;
            ContractVersion = contractVersion;
        }
        #endregion
        public virtual string Name { get; set; } // [711-721-731]
        public virtual decimal Value { get; set; } // [712-722-732]
        public virtual DateTime StartDate { get; set; } // [713-723-733]
        public virtual DateTime EndDate { get; set; } // [714-724-734]
        public virtual IContractVersion ContractVersion
        {
            get;
            set;
        }

    }
}
