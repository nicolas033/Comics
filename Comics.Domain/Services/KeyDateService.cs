﻿using Comics.Domain.Entities;
using Comics.Domain.Entities.Common;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IKeyDateService
    {
        void CreateKeyDate(Guid contractVersionId, string name, KeyDateType type, DateTime targetDate, DateTime limitDate, DateTime unitDelay, decimal unitPrice, decimal cap, DateTime actualDate);
        void UpdateKeyDate(Guid keyDateId, string name, KeyDateType type, DateTime targetDate, DateTime limitDate, DateTime unitDelay, decimal unitPrice, decimal cap, DateTime actualDate);
        void DeleteKeyDate(Guid keyDateId);
    }

    public class KeyDateService : IKeyDateService
    {
        IRepository repository;
        public KeyDateService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateKeyDate(Guid contractVersionId, string name, KeyDateType type, DateTime targetDate, DateTime limitDate, DateTime unitDelay, decimal unitPrice, decimal cap, DateTime actualDate)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contractVersionId);
            KeyDate KeyDate = new KeyDate(name, type, targetDate, limitDate, unitDelay, unitPrice, cap, actualDate, currentContractVersion);
            repository.Add(KeyDate);
        }

        public void UpdateKeyDate(Guid keyDateId, string name, KeyDateType type, DateTime dueDate, DateTime limitDate, DateTime unitDelay, decimal unitPrice, decimal cap, DateTime actualDate)
        {
            var currentKeyDate = repository.GetById<IKeyDate>(keyDateId);
            currentKeyDate.Name = name;
            currentKeyDate.Type = type;
            currentKeyDate.DueDate = dueDate;
            currentKeyDate.LimitDate = limitDate;
            currentKeyDate.UnitDelay = unitDelay;
            currentKeyDate.UnitPrice = unitPrice;
            currentKeyDate.CAP = cap;
            currentKeyDate.ActualDate = actualDate;
            repository.Add(currentKeyDate);
        }

        public void DeleteKeyDate(Guid keyDateId)
        {
            var currentKeyDate = repository.GetById<IKeyDate>(keyDateId);
            repository.Remove(currentKeyDate);
        }

    }
}
