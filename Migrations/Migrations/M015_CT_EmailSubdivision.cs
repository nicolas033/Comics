﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(015)]
    public class M015_CT_EmailSubdivision : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("emailsubdivision")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_emailsubdivision")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("email").AsString()
                 .WithColumn("tocc").AsString()
                 .WithColumn("subdivisionid").AsGuid().ForeignKey("fk_emailsubdivision_subdivision", "subdivision", "id")
                 .WithColumn("actorid").AsGuid().ForeignKey("fk_emailsubdivision_actor","actor", "id");       
        }
    }
}
