﻿using Autofac;
using System;
using System.IO;
using System.Reflection;

using Autofac.Extensions.DependencyInjection;
using Comics.Domain.Services;
using Comics.Domain.NHibernate;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Autofac.Integration.WebApi;
using Microsoft.AspNetCore.Http;
using System.Web.Mvc;
using Autofac.Integration.Mvc;
using System.Web.Http;

namespace Comics.WebAPI
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
        .SetBasePath(env.ContentRootPath)
        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
        .AddJsonFile($"appsettings.json", optional: true)
        .AddEnvironmentVariables();
            this.Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        // IContainer instance in the Startup class 
        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Register all assemblies in which MVC can find controllers/views in the ApplicationPartManager
            // This is required for environments where .NET Core 2.1.300 SDK or later is installed
            // https://github.com/aspnet/Home/issues/3132#issuecomment-388082725
            //var manager = new ApplicationPartManager();
            //manager.ApplicationParts.Add(new AssemblyPart(typeof(Startup).Assembly));
            //services.AddSingleton(manager);
             //services.AddScoped<IRepository, NHibernateRepository>();
            //services.AddTransient(sp => Configuration);
            services.AddMvc();
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            // services.AddScoped(sp => sp.GetRequiredService<IHttpContextAccessor>().HttpContext);
            // services.AddScoped<IRepository, NHibernateRepository>();
            //services.AddScoped(sp => sp.GetRequiredService<IHttpContextAccessor>().HttpContext);
            //services.AddScoped<IRepository, NHibernateRepository>();
            // services.AddScoped<IContractService,ContractService>();
            // services.AddTransient(ctx =>
            //new HomeController(new ContractService()));

            SessionFactoryConfiguration.InitializeSessionFactory(GetConnectionString());
           // Map.Initialize(new GenericMapperLocator());

            ISessionContext sessionContext = new StaticSessionContext();
            NHibernateHelperMultiThread.Initialize(sessionContext, SessionFactoryConfiguration.SessionFactory);

            // create a Autofac container builder
            var builder = new ContainerBuilder();
            builder.Populate(services);

            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>();
            builder.RegisterType<NHibernateRepository>().As<IRepository>();
            builder.RegisterType<ContractService>().As<IContractService>();
            builder.RegisterType<ContractVersionService>().As<IContractVersionService>();
            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            //builder.RegisterType<ContractController>();
            //builder.RegisterType<ValuesController>();
            //services.AddScoped<IRepository, NHibernateRepository>();
            //services.AddScoped<NHibernateRepositoryFilter>();

            //builder.RegisterType<NHibernateRepository>().As<IRepository>();

            //        builder.RegisterGeneric(typeof(NHibernateRepository))
            //.As(typeof(IRepository));
        

        
           
            //services.AddScoped<ICacheRepository, NHibernateRepository>();
           
            // use and configure Autofac
            // builder.RegisterType<ContractService>().As<IContractService>();
            // read service collection to Autofac
            //builder.Populate(services);



            // build the Autofac container
            this.ApplicationContainer = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(this.ApplicationContainer));
       //     builder.RegisterType<ContractService>().As<IContractService>()
       //.SingleInstance().PreserveExistingDefaults();

            //DependencyResolver.Resolver.
            //DependencyResolver.SetResolver(this.ApplicationContainer);
            // builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // creating the IServiceProvider out of the Autofac container
            var serviceProvider = new AutofacServiceProvider(ApplicationContainer);
          //  serviceProvider.GetService<IRepository>();
            return serviceProvider;

            //new ConfigPropertyManager(serviceProvider.GetService<IRepository>());

            //Mapping.Mapping.Initialize();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors(builder => builder
             .AllowAnyOrigin()
             .AllowAnyHeader()
             .AllowAnyMethod()
             .AllowCredentials()
         );
            app.UseHttpsRedirection();
            //            app.UseMvc(routes =>
            //            {
            //                routes.MapRoute(
            //    name: "API Default",
            //    template: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
            //            })
            //            ;
            //        }

            app.UseMvc();

       //     app.UseMvc(routes =>
       //     {

       //         routes.MapRoute(
       //name: "default",
       //template: "{controller=Home}/{action=Index}/{id?}");
       //     });
        }

        public static IConfiguration DbConfiguration;
        private static string GetConnectionString()
        {
            var configurationBuilder = new ConfigurationBuilder()
         .SetBasePath(Directory.GetCurrentDirectory())
         .AddJsonFile("appsettings.json", optional: false);

            DbConfiguration = configurationBuilder.Build();
            var connectionString = DbConfiguration["ConnectionStrings:ComicsDBConnectionString"];

            return connectionString;
        }
    }
}
