import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractVersionComponent } from './components/contract-version/contract-version.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ContractComponent } from './components/contract/contract.component';
import { ContractListComponent } from './components/contract-list/contract-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'contracts',
    component: ContractListComponent
  },
  {
    path: 'contractversions',
    component: ContractVersionComponent
  },
  {
    path: 'contract/:reference',
    component: ContractComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppModuleRoutes {
}
