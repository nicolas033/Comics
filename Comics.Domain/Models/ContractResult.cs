﻿using Comics.Domain.Entities.Common;
using System.Collections.Generic;

namespace Comics.Domain.Models
{
    public class ContractResult
    {
        public List<MappedContract> MappedContracts = new List<MappedContract>();
    }
    public class MappedContract
    {
        public string Reference { get; set; }
        public string SignatureDate { get; set; }
        public List<MappedContractVersion> ContractVersions {get;set; }
        public int NumberOfContractVersions { get; set; }
    }
    public class MappedContractVersion
    {
        public int Version { get; set; }
        public ContractType Type { get; set; }
        public string Name { get; set; }
        public string CreationDate { get; set; }
    }
}
