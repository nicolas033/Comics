using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using NHibernate;
using ISession = NHibernate.ISession;

namespace Comics.Domain.NHibernate
{
    public class NHibernateRepository : IRepository
    {
        private readonly IHttpContextAccessor contextAccessor;
        protected ITransaction transaction = null;
        public NHibernateRepository(IHttpContextAccessor contextAccessor)          
        {
            this.contextAccessor = contextAccessor;
        }

        protected virtual ISession Session
        {
            get
            {
                return NHibernateHelperMultiThread.GetCurrentSession(contextAccessor.HttpContext.TraceIdentifier);
            }
        }

        #region IRepository Members
        public void Add<T>( T t ) where T : IAggregateRoot
        {
            transaction = Session.BeginTransaction();
            Session.Save( t );
            transaction.Commit();
        }

        public void AddNonRoot<T>(T t) where T : IAggregateRoot
        {
            Session.Save( t );
        }

        public void Update<T>(T t) where T : IAggregateRoot
        {
            transaction = Session.BeginTransaction();
            Session.Update(t);
            transaction.Commit();
        }


        public void Remove<T>( T t ) where T : IAggregateRoot
        {
            transaction = Session.BeginTransaction();
            Session.Delete(t);
            transaction.Commit();         
        }

        public void RemoveNonRoot<T>( T t ) where T : IAggregateRoot    
        {
            Session.Delete( t );
        }

        public IQueryResult<T> Query<T>(IQuery<T> query)
        {
            return query.Execute(Session);
        }
        public IList<T> GetAll<T>()
        {

            return Session.Query<T>().ToList();
        }

        public void ClearFromCache( object objectToClear )
        {
            Session.Flush();
            Session.Evict( objectToClear );
        }

        public T GetById<T>(Guid id) where T : IAggregateRoot
        {
            return Session.Get<T>(id);
        }
        #endregion
    }
}