﻿using Comics.Domain.Services;
using Comics.Domain.NHibernate;
using Microsoft.AspNetCore.Mvc;
using Comics.Domain.Models;
using Comics.WebAPI.Requests;
using System;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using System.Net;

namespace Comics.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ContractController : ControllerBase
    {
        private readonly IRepository repository;
        private readonly IContractService contractService;
        private readonly IContractVersionService contractVersionService;
   
        public ContractController(IContractService contractService,IContractVersionService contractVersionService)
        {
            this.contractService = contractService;
            this.contractVersionService = contractVersionService;
        }

        [HttpGet]
        public ContractResult GetAll()
        {        
            var contracts = this.contractService.GetAllContracts();
            return contracts;
        }
        [HttpGet]
        public MappedContract GetById(Guid id)
        {
            var contract = contractService.GetContract(id);

            return contract;

        }

        [HttpPost]
        public ActionResult Create([FromBody] ContractCreateRequest contractData)
        {
            var reference =  contractData.Reference;         
            var creationDate = contractData.SignatureDate;
            contractService.CreateContract(reference, creationDate);
            var request = new HttpRequestMessage();
            return Ok();
        }

        [HttpDelete]
        public ActionResult Delete(Guid id)
        {

            contractService.DeleteContract(id);

            return Ok("Contract successfully deleted");
        }

        [HttpPut]
        public ActionResult Update(Guid id, [FromBody] ContractUpdateRequest contractData)
        {
            var name = contractData.Name;

            contractService.UpdateContract(id);

            return Ok("Contract successfully updated");
        }

    }
}
