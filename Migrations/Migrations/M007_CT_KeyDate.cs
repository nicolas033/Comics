﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(007)]
    public class M007_CT_KeyDate : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("keydate")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_keydate")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("name").AsString()
                 .WithColumn("type").AsString()
                 .WithColumn("duedate").AsDateTime()
                 .WithColumn("limitdate").AsDateTime()
                 .WithColumn("unitdelay").AsDateTime()
                 .WithColumn("unitprice").AsDecimal()
                 .WithColumn("cap").AsDecimal()
                 .WithColumn("actualdate").AsDateTime()
                 .WithColumn("contractversionid").AsGuid().ForeignKey("fk_keydate_contractversion", "contractversion", "id");
        }
    }
}
