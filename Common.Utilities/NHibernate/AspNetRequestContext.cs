using Microsoft.AspNetCore.Http;

namespace Common.Utilities.NHibernate
{
    public class AspNetRequestContext : ISessionContext
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public AspNetRequestContext(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public object this[string key]
        {
            get => httpContextAccessor.HttpContext.Items.ContainsKey(key) ? httpContextAccessor.HttpContext.Items[key] : null;
            set => httpContextAccessor.HttpContext.Items[key] = value;
        }
    }
}