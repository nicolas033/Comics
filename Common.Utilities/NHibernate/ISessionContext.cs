﻿namespace Common.Utilities.NHibernate
{
    public interface ISessionContext
    {
        object this[string key]
        {
            get;
            set;
        }
    }
}
