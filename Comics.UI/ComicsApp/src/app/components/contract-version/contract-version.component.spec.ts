import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractVersionComponent } from './contract-version.component';

describe('ContractVersionComponent', () => {
  let component: ContractVersionComponent;
  let fixture: ComponentFixture<ContractVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
