﻿using System;
using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IPaymentSchedule : IEntity
    {     
        string Name { get; set; }
        decimal Value { get; set; } //ok
        string Currency { get; set; } //ok what is this ?
        string InvoicingCondition { get; set; } //ok
        string ScheduleCondition { get; set; } //ok
        DateTime PaymentDate { get; set; } //ok
        IContractVersion ContractVersion { get; set; }
    }


    public class PaymentSchedule : Entity, IPaymentSchedule
    {
        protected PaymentSchedule()
        {

        }
        public PaymentSchedule(string name, decimal value, string currency, string invoicingCondition, string scheduleCondition, DateTime paymentDate, IContractVersion contractVersion)
        {
            Name = name;
            Value = value;
            Currency = currency;
            InvoicingCondition = invoicingCondition;
            ScheduleCondition = scheduleCondition;
            PaymentDate = paymentDate;
            ContractVersion = contractVersion;
        }

        public virtual string Name { get; set; }
        public virtual decimal Value { get; set; }
        public virtual string Currency { get; set; }
        public virtual string InvoicingCondition { get; set; }
        public virtual string ScheduleCondition { get; set; }
        public virtual DateTime PaymentDate { get; set; }
        public virtual IContractVersion ContractVersion { get; set; }
    }
}
