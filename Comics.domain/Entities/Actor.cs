﻿using System.Collections.Generic;
using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IActor : IEntity
    {
        string Name { get; set; }
        string CommunicationCode { get; set; }
        string Country { get; set; }
        string Address { get; set; }
        string Email { get; set; }
        ActorType Type { get; set; }
        IContractVersion ContractVersion { get; set; }
        IEnumerable<IRepresentation> Representations { get;}
    }
        
    public class Actor : Entity, IActor
    {
        protected ISet<IRepresentation> representations;
        protected Actor()
        {
            representations = new HashSet<IRepresentation>();
        }
        public Actor(string name, string communicationCode, string country, string address, string email, ActorType type, IContractVersion contractVersion)
        {
            Name = name;
            CommunicationCode = communicationCode;
            Country = country;
            Address = address;
            Email = email;
            Type = type;
            ContractVersion = contractVersion;
        }
        public virtual string Name { get; set; } // [15]
        public virtual string CommunicationCode { get; set; }
        public virtual string Country { get; set; }
        public virtual string Address { get; set; }      
        public virtual string Email { get; set; }
        public virtual ActorType Type { get; set; }
        public virtual IContractVersion ContractVersion { get; set; }
        public virtual IEnumerable<IRepresentation> Representations => representations;
    }
}
