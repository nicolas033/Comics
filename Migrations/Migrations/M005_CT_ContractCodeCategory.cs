﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(005)]
    public class M005_CT_ContractCodeCategory : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("contractcodecategory")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_contractcodecategory")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("name").AsString()
                 .WithColumn("code").AsString()
                .WithColumn("contractversionid").AsGuid().ForeignKey("fk_contractcodecategory_contractversion", "contractversion", "id");
        }
    }
}
