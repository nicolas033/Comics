import { Component } from '@angular/core';
import { formatDate } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'Comics';
  today = new Date();
  jstoday = '';
  name = 'Angular 4';
  private _opened: boolean = true;

  private _toggleSidebar() {
    this._opened = !this._opened;
  }
  constructor() {
    this.jstoday = formatDate(this.today, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
  }
}
