﻿using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IEmailSubdivision : IEntity
    {
        string Email { get; set; }
        string ToCc { get; set; }
        IActor Actor { get; set; }
        ISubdivision Subdivision { get; set; }
    }

    public class EmailSubdivision : Entity,IEmailSubdivision
    {
        protected EmailSubdivision()
        {
        }
        public EmailSubdivision(string email, string toCc, IActor actor, ISubdivision subdivision)
        {
            Email = email;
            ToCc = toCc;
            Actor = actor;
            Subdivision = subdivision;
        }
        public virtual string Email { get; set; }
        public virtual string ToCc { get; set; }
        public virtual IActor Actor { get; set; }
        public virtual ISubdivision Subdivision { get; set; }
    }
}
