﻿using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IRepresentation : IEntity
    {
        string Title { get; set; }
        string Email { get; set; }
        IActor Actor { get; set; }
    }

    public class Representation : Entity,IRepresentation
    {

        protected Representation()
        {

        }
        public Representation(string title, string email, IActor actor)
        {
            Title = title;
            Email = email;
            Actor = actor;
        }
        public virtual string Title { get; set; }
        public virtual string Email { get; set; }
        public virtual IActor Actor { get; set; }
    }
}
 