﻿using Microsoft.Extensions.DependencyInjection;
using System;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Processors;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using System.IO;


namespace Comics.Migrations
{

    public class Program
    {
        public static IConfigurationRoot Configuration;
       
        public static void Main(string[] args)
        {
            var serviceProvider = CreateServices();

            // Put the database update into a scope to ensure
            // that all resources will be disposed.
            using (var scope = serviceProvider.CreateScope())
            {
                UpdateDatabase(scope.ServiceProvider);
            }
        }
        private static string GetConnectionString()
        {
            var configurationBuilder = new ConfigurationBuilder()
         .SetBasePath(Directory.GetCurrentDirectory())
         .AddJsonFile("appsettings.json", optional: false);

            Configuration = configurationBuilder.Build();
            var connectionString = Configuration["ConnectionStrings:ComicsDBConnectionString"];

            return connectionString;
        }
        /// <summary>
        /// Configure the dependency injection services
        /// </sumamry>
        private static IServiceProvider CreateServices()
        {

            var connectionString = GetConnectionString();
            var obj = new ServiceCollection();
            var assembly = Assembly.GetExecutingAssembly();
            return new ServiceCollection()
                .AddFluentMigratorCore().
                ConfigureRunner(builder => builder.AddPostgres()
                // Define the assembly containing the migrations
                .ScanIn(assembly).For.Migrations()).
                Configure<ProcessorOptions>(x => x.ConnectionString
                = /*"Server=localhost;Port=5432;Database=Comics;User Id=postgres;Password=Admin123;"*/
               connectionString)
                  .AddLogging(lb => lb.AddFluentMigratorConsole())
                    // Build the service provider
                    .BuildServiceProvider();
        }

        /// <summary>
        /// Update the database
        /// </sumamry>
        private static void UpdateDatabase(IServiceProvider serviceProvider)
        {
            // Instantiate the runner
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();

            // Execute the migrations
            runner.MigrateUp();
        }
    }
   
}
