﻿using System;
using Comics.Domain.Entities.Common;

namespace Comics.Domain.Entities
{
    public interface IKeyDateStatus : IEntity
    {
        DateTime EstimationDate { get; set; }//this can go to keydate, should be ActualDate
        DateTime BestEstimate { get; set; }//this can go to keydate
        IKeyDate KeyDate { get; set; }
    }

    public class KeyDateStatus : Entity, IKeyDateStatus
    {
        protected KeyDateStatus()
        {
          
        }
        public KeyDateStatus(DateTime estimationDate, DateTime bestEstimate, IKeyDate keyDate)
        {
            EstimationDate = estimationDate;
            BestEstimate = bestEstimate;
            KeyDate = keyDate;
        }
        public virtual DateTime EstimationDate { get; set; }
        public virtual DateTime BestEstimate { get; set; }
        public virtual IKeyDate KeyDate { get; set; }
    }
}
