﻿using Murmur;
using System.Security.Cryptography;
using System.Text;

namespace Comics.Domain.Common
{
    public static class Helper
    {
        public static string GetSha256Hash(SHA256 shaHash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = shaHash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static string GetHash(string concatenatedString)
        {
            var contractByteArray = Encoding.ASCII.GetBytes(concatenatedString);
            var murmur128 = MurmurHash.Create128(0, false);
            var hashByteArray = murmur128.ComputeHash(contractByteArray);
            var hashString = Encoding.Default.GetString(hashByteArray);

            return hashString;
        }

    }
}