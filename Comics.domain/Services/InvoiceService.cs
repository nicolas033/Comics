﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IInvoiceService
    {
        void CreateInvoice(Guid paymentMSId, string reference, string type, decimal value, DateTime invoiceDate, DateTime dueDate, string status, decimal pourcentagePaid, decimal pourcentageMSAchieved);
        void UpdateInvoice(Guid invoiceId, string reference, string type, decimal value, DateTime invoiceDate, DateTime dueDate, string status, decimal pourcentagePaid, decimal pourcentageMSAchieved);
        void DeleteInvoice(Guid invoiceId);
    }

    public class InvoiceService : IInvoiceService
    {
        IRepository repository;
        public InvoiceService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateInvoice(Guid paymentMSId, string reference, string type, decimal value, DateTime invoiceDate, DateTime dueDate, string status, decimal pourcentagePaid, decimal pourcentageMSAchieved)
        {
            var currentPayment = repository.GetById<IPaymentSchedule>(paymentMSId);
            Invoice Invoice = new Invoice(reference, type, value, invoiceDate, dueDate, status, currentPayment, pourcentagePaid, pourcentageMSAchieved);
            repository.Add(Invoice);
        }

        public void UpdateInvoice(Guid invoiceId, string reference, string type, decimal value, DateTime invoiceDate, DateTime dueDate, string status, decimal pourcentagePaid, decimal pourcentageMSAchieved)
        {
            var currentInvoice = repository.GetById<IInvoice>(invoiceId);
            currentInvoice.Reference = reference;
            currentInvoice.Type = type;
            currentInvoice.Value = value;
            currentInvoice.Date = invoiceDate;
            currentInvoice.DueDate = dueDate;
            currentInvoice.Status = status;
            currentInvoice.PercentagePaid = pourcentagePaid;
            currentInvoice.PercentageMSAchieved = pourcentageMSAchieved;
            repository.Add(currentInvoice);
        }

        public void DeleteInvoice(Guid invoiceId)
        {
            var currentInvoice = repository.GetById<IInvoice>(invoiceId);
            repository.Remove(currentInvoice);
        }

    }
}
