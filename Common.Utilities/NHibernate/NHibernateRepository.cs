using Microsoft.AspNetCore.Http;
using ISession = NHibernate.ISession;

namespace Common.Utilities.NHibernate
{
    public class NHibernateRepository : IRepository
    {
        private readonly IHttpContextAccessor contextAccessor;

        public NHibernateRepository(IHttpContextAccessor contextAccessor)          
        {
            this.contextAccessor = contextAccessor;
        }

        protected virtual ISession Session
        {
            get
            {
                return NHibernateHelperMultiThread.GetCurrentSession(contextAccessor.HttpContext.TraceIdentifier);
            }
        }

        public void Add<T>( T t ) where T : IAggregateRoot
        {
            Session.Save( t );
        }

        public void AddNonRoot<T>(T t)
        {
            Session.Save( t );
        }

        public void Remove<T>( T t ) where T : IAggregateRoot
        {
            Session.Delete( t );
        }

        public void RemoveNonRoot<T>( T t )
        {
            Session.Delete( t );
        }

        public IQueryResult<T> Query<T>( IQuery<T> query )
        {
            return query.Execute( Session );
        }

        public void ClearFromCache( object objectToClear )
        {
            Session.Flush();
            Session.Evict( objectToClear );
        }
    }
}