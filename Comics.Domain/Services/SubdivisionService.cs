﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface ISubdivisionService
    {
        void CreateSubdivision(Guid contractVersionId, string title);
        void UpdateSubdivision(Guid subdivisionId, string title);
        void DeleteSubdivision(Guid subdivisionId);
    }

    public class SubdivisionService : ISubdivisionService
    {
        IRepository repository;
        public SubdivisionService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateSubdivision(Guid contractVersionId, string title)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contractVersionId);
            Subdivision Subdivision = new Subdivision(title, currentContractVersion);
            repository.Add(Subdivision);
        }

        public void UpdateSubdivision(Guid subdivisionId, string title)
        {
            var currentSubdivision = repository.GetById<ISubdivision>(subdivisionId);
            currentSubdivision.Title = title;
            repository.Add(currentSubdivision);
        }

        public void DeleteSubdivision(Guid subdivisionId)
        {
            var currentSubdivision = repository.GetById<ISubdivision>(subdivisionId);
            repository.Remove(currentSubdivision);
        }

    }
}
