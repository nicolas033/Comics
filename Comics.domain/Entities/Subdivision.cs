﻿using Comics.Domain.Entities.Common;
using System.Collections.Generic;

namespace Comics.Domain.Entities
{
    public interface ISubdivision : IEntity
    {
        string Title { get; set; }
        IContractVersion ContractVersion { get; set; }
        IEnumerable<IEmailSubdivision> EmailSubdivisions { get; }
    }

    public class Subdivision : Entity,ISubdivision
    {
        protected ISet<IEmailSubdivision> emailSubdivisions;
        protected Subdivision()
        {
            emailSubdivisions = new HashSet<IEmailSubdivision>();
        }
        public Subdivision(string title, IContractVersion contractVersion)
        {
            Title = title;
            ContractVersion = contractVersion;
        }

        public virtual string Title { get; set; }
        public virtual IContractVersion ContractVersion { get; set; }
        public virtual IEnumerable<IEmailSubdivision> EmailSubdivisions => emailSubdivisions;
    }
}
