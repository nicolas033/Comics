﻿using Comics.Domain.Entities.Common;
using System;
using System.Collections.Generic;

namespace Comics.Domain.Entities
{
    public interface IContractVersion : IEntity
    {
        IContract Contract { get; set; } //ok
        int Version { get; set; } //ok
        ContractType Type { get; set; } //ok
        string Name { get; set; } //ok
        //DateTime EffectiveDate { get; set; } //nok
        //string ClientName { get; set; } //ok
        //string ContractorName { get; set; } //ok
        IEnumerable<IActor> Actors { get; }
        IEnumerable<ISubdivision> ProtcolSubdivisions { get; }
        IEnumerable<IOption> Options { get; }
        IEnumerable<IContractCodeCategory> ContractCodeCategories { get; }
        IEnumerable<IBond> Bonds { get; }
        IEnumerable<IPrice> Prices { get; }
        IEnumerable<IGuaranteedValue> GuaranteedValues { get; }
        //IEnumerable<IKeyDate> KeyDates { get; }
        IEnumerable<IPaymentSchedule> PaymentsSchedule { get; }
    }

    public class ContractVersion : Entity, IContractVersion
    {
        protected ISet<IActor> actors;
        protected ISet<ISubdivision> protcolSubdivisions;
        protected ISet<IOption> options;
        protected ISet<IContractCodeCategory> contractCodeCategories;
        protected ISet<IBond> bonds;
        protected ISet<IPrice> prices;
        //protected ISet<IPrice> optionsPrices;
        protected ISet<IGuaranteedValue> guaranteedValues;
        protected ISet<IKeyDate> keyDates;
        //protected ISet<IKeyDate> clientKeyDates;
        //protected ISet<IKeyDate> contractorKeyDates;
        protected ISet<IPaymentSchedule> paymentsSchedule;
        protected ContractVersion()
        {
            actors = new HashSet<IActor>();
            protcolSubdivisions = new HashSet<ISubdivision>();
            options = new HashSet<IOption>();
            contractCodeCategories = new HashSet<IContractCodeCategory>();
            bonds = new HashSet<IBond>();
            prices = new HashSet<IPrice>();
            //optionsPrices = new HashSet<IPrice>();
            guaranteedValues = new HashSet<IGuaranteedValue>();
            //keyDates = new HashSet<IKeyDate>();
            //clientKeyDates = new HashSet<IKeyDate>();
            //contractorKeyDates = new HashSet<IKeyDate>();
            paymentsSchedule = new HashSet<IPaymentSchedule>();
        }
        public ContractVersion(IContract contract, int version, ContractType type, string name, string contractVersionHash)
        {
            Contract = contract;
            Type = type;
            Name = name;
            Version = version;
            CreationDateTime = DateTime.UtcNow;
            Hash = contractVersionHash;
        }
        public virtual IContract Contract { get; set; }
        public virtual int Version { get; set; }
        public virtual ContractType Type { get; set; } // [11]
        public virtual string Name { get; set; } // [12]
        // not needed : sum of all base price -  public virtual Decimal TotalBasePrice { get; set; } // [81]
        // not needed : sum of all option prices - public virtual Decimal TotalOptionPrice { get; set; } // [82]
        //public virtual string ContractCode { get; set; } // [] Example G1 
        //public virtual string ClientCode { get; set; }
        //public virtual string ContractorCode { get; set; }
        //public virtual string CAPTotalLiquidatedDamages { get; set; }  // [6]
        //public virtual string CAPTotalDelay { get; set; }
        //public virtual string CAPTotalClientDelay { get; set; } // [61]
        //public virtual string CAPTotalContarctorDelay { get; set; } // [62] 
        //public virtual string CAPTotalGuaranteedValue { get; set; } // [63]
        //public virtual string CAPTotal { get; set; }
        public virtual IEnumerable<IActor> Actors => actors;
        public virtual IEnumerable<ISubdivision> ProtcolSubdivisions => protcolSubdivisions;
        public virtual IEnumerable<IOption> Options => options;
        public virtual IEnumerable<IContractCodeCategory> ContractCodeCategories => contractCodeCategories;
        public virtual IEnumerable<IBond> Bonds => bonds;
        public virtual IEnumerable<IPrice> Prices => prices;
        //public virtual IEnumerable<IPrice> OptionsPrices => optionsPrices;
        public virtual IEnumerable<IGuaranteedValue> GuaranteedValues => guaranteedValues;
       // public virtual IEnumerable<IKeyDate> KeyDates => KeyDates;
        //public virtual IEnumerable<IKeyDate> ClientKeyDates => clientKeyDates;
        //public virtual IEnumerable<IKeyDate> ContractorKeyDates => contractorKeyDates;
        public virtual IEnumerable<IPaymentSchedule> PaymentsSchedule => paymentsSchedule;
    }
}
