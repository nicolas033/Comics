﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(014)]
    public class M014_CT_Subdivision : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("subdivision")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_subdivision")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("title").AsString()
                 .WithColumn("contractversionid").AsGuid().ForeignKey("fk_subdivision_contractversion", "contractversion", "id");
        }
    }
}
