﻿using Comics.Domain.Common;
using Comics.Domain.Entities;
using Comics.Domain.Entities.Common;
using Comics.Domain.NHibernate;
using System;
using System.Linq;

namespace Comics.Domain.Services
{
    public interface IContractVersionService
    {
        void CreateContractVersion(Guid contractId, ContractType type, string name, DateTime signatureDate);
        void UpdateContractVersion(Guid contratVersionId, ContractType type, string name, DateTime signatureDate);
        void DeleteContractVersion(Guid contratVersionId);
    }

    public class ContractVersionService : IContractVersionService
    {
        private IRepository repository;

        public ContractVersionService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateContractVersion(Guid contractId, ContractType type, string name, DateTime versionDate)
        {
            var currentContract = repository.GetById<IContract>(contractId);
            var version = currentContract.Versions.ToList().Count + 1;

            // var contratVersionHash = Helper.GetHash(string.Concat(type, name, versionDate));
            var contratVersionHash = "ContractVersionHash";
            var contractVersion = new ContractVersion(currentContract, version, type, name, contratVersionHash);

            currentContract.AddContractVersion(contractVersion);
            repository.Add(contractVersion);
        }
        public void UpdateContractVersion(Guid contratVersionId, ContractType type, string name, DateTime versionDate)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contratVersionId);
            currentContractVersion.Type = type;
            currentContractVersion.Name = name;
            //currentContractVersion.CreationDateTime = versionDate;

            var newContratVersionHash = Helper.GetHash(string.Concat(type, name, versionDate));
            currentContractVersion.Hash = newContratVersionHash;
            repository.Update(currentContractVersion);
        }
        public void DeleteContractVersion(Guid id)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(id);
            repository.Remove(currentContractVersion);
        }
    }
}
