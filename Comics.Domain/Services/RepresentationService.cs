﻿using Comics.Domain.Entities;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IRepresentationService
    {
        void CreateRepresentation(Guid actorId, string title, string email);
        void UpdateRepresentation(Guid representationId, string title, string email);
        void DeleteRepresentation(Guid representationId);
    }

    public class RepresentationService : IRepresentationService
    {
        IRepository repository;
        public RepresentationService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreateRepresentation(Guid actorId, string title, string email)
        {
            var currentActor = repository.GetById<IActor>(actorId);
            Representation Representation = new Representation(title, email, currentActor);
            repository.Add(Representation);
        }

        public void UpdateRepresentation(Guid representationId, string title, string email)
        {
            var currentRepresentation = repository.GetById<IRepresentation>(representationId);
            currentRepresentation.Title = title;
            currentRepresentation.Email = email;
            repository.Add(currentRepresentation);
        }

        public void DeleteRepresentation(Guid representationId)
        {
            var currentRepresentation = repository.GetById<IRepresentation>(representationId);
            repository.Remove(currentRepresentation);
        }

    }
}
