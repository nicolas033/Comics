﻿using FluentMigrator;

namespace Comics.Migrations
{
    [Migration(006)]
    public class M006_CT_GuaranteedValue : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("guaranteedvalue")
                 .WithColumn("id").AsGuid().PrimaryKey("pk_guaranteedvalue")
                 .WithColumn("creationdatetime").AsDateTime()
                 .WithColumn("hash").AsString()
                 .WithColumn("name").AsString()
                 .WithColumn("unit").AsDecimal()
                 .WithColumn("targetvalue").AsString()
                 .WithColumn("lowerlimit").AsDecimal()
                 .WithColumn("upperlimit").AsDecimal()
                 .WithColumn("actualvalue").AsString()
                 .WithColumn("unitdeviation").AsDecimal()
                 .WithColumn("unitprice").AsDecimal()
                 .WithColumn("cap").AsDecimal()
                 .WithColumn("contractversionid").AsGuid().ForeignKey("fk_guaranteedvalue_contractversion", "contractversion", "id");
        }
    }
}
