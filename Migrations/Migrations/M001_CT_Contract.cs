﻿using FluentMigrator;

namespace Migrations
{
    [Migration(001)]
    public class M001_CT_Contract : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("contract")
                .WithColumn("id").AsGuid().PrimaryKey("pk_contract")
                .WithColumn("creationdatetime").AsDateTime()
                .WithColumn("hash").AsString()
                .WithColumn("reference").AsString();
        }

    }
}
