﻿using System;

namespace Comics.WebAPI.Requests
{
    public class ContractCreateRequest
    {
        public string Reference;
        public DateTime SignatureDate;
      
    }
}
