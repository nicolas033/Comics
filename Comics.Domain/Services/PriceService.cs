﻿using Comics.Domain.Entities;
using Comics.Domain.Entities.Common;
using Comics.Domain.NHibernate;
using System;

namespace Comics.Domain.Services
{
    public interface IPriceService
    {
        void CreatePrice(Guid contractVersionId, string name, PriceType type);
        void UpdatePrice(Guid priceId, string name, PriceType type);
        void DeletePrice(Guid priceId);
    }

    public class PriceService : IPriceService
    {
        IRepository repository;
        public PriceService(IRepository repository)
        {
            this.repository = repository;
        }

        public void CreatePrice(Guid contractVersionId, string name, PriceType type)
        {
            var currentContractVersion = repository.GetById<IContractVersion>(contractVersionId);
            Price Price = new Price(name, type, currentContractVersion);
            repository.Add(Price);
        }

        public void UpdatePrice(Guid priceId, string name, PriceType type)
        {
            var currentPrice = repository.GetById<IPrice>(priceId);
            currentPrice.Name = name;
            currentPrice.Type = type;
            repository.Add(currentPrice);
        }

        public void DeletePrice(Guid priceId)
        {
            var currentPrice = repository.GetById<IPrice>(priceId);
            repository.Remove(currentPrice);
        }

    }
}
